const URL = "https://v3.tissini.app/api/v3/";

function getAllCategories() {
  return fetch(`${URL}categories`)
    .then(res => res.json())
    .then(res => res.categories);
}

function getAllSections() {
  return fetch(`${URL}categories/sections`).then(res => res.json());
}

function getCategoriesIdProducts(id) {
  return fetch(`${URL}categories/${id}/products`)
    .then(res => res.json())
    .then(res => res.products);
}

export default {
  getAllCategories,
  getAllSections,
  getCategoriesIdProducts
};
